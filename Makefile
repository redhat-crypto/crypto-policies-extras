CFLAGS+=-g
LDLIBS+=-lcap-ng

ASCIIDOC?=asciidoc
ifneq ("$(wildcard /usr/lib/python*/*/asciidoc/resources/docbook-xsl/manpage.xsl)","")
MANPAGEXSL?=$(wildcard /usr/lib/python*/*/asciidoc/resources/docbook-xsl/manpage.xsl)
else
MANPAGEXSL?=/usr/share/asciidoc/docbook-xsl/manpage.xsl
endif

all: runcp runcp.8

runcp.8: runcp.8.txt
	$(ASCIIDOC) -v -d manpage -b docbook $<
	xsltproc --nonet -o $@ ${MANPAGEXSL} $@.xml

PHONY: clean
clean:
	rm -f runcp runcp.8 runcp.8.xml

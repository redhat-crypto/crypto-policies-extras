// SPDX-License-Identifier:  GPL-3.0-or-later

#define _GNU_SOURCE

#include <cap-ng.h>
#include <sched.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/mount.h>
#include <sys/wait.h>
#include <unistd.h>

#define TMPDIR_PATTERN	"/tmp/runcp-XXXXXX"

static void run_cmd(char **const argv, char **const envp)
{
	pid_t pid;
	int status;

	pid = fork();
	if (pid == -1) {
		fprintf(stderr, "Failed to create child process: %s: %m\n",
			argv[0]);
		exit(EXIT_FAILURE);
	}

	if (pid == 0) {

		execvpe(argv[0], argv, envp);

		fprintf(stderr, "Failed to execute child command: %s: %m\n",
			argv[0]);
		exit(EXIT_FAILURE);
	}
	else {

		if (waitpid(pid, &status, 0) != pid) {
			fprintf(stderr,
				"Failed to wait for child process: %s: %m\n",
				argv[0]);
			exit(EXIT_FAILURE);
		}

		if (!WIFEXITED(status) || WEXITSTATUS(status) != 0) {
			fprintf(stderr, "Child process failed: %s\n", argv[0]);
			exit(EXIT_FAILURE);
		}
	}
}

static void set_admin(const capng_act_t action)
{
	static const char *actions[] = {
		[CAPNG_DROP] = "drop", [CAPNG_ADD] = "add"
	};

	if (capng_update(action, CAPNG_EFFECTIVE, CAP_SYS_ADMIN) != 0) {
		fprintf(stderr, "Failed to %s CAP_SYS_ADMIN: %m\n",
			actions[action]);
		exit(EXIT_FAILURE);
	}

	if (capng_apply(CAPNG_SELECT_CAPS) != 0) {
		fprintf(stderr, "Failed to apply capabilities: %m\n");
		exit(EXIT_FAILURE);
	}
}

int main(const int argc, char **const argv)
{
	char tmpdir[] = TMPDIR_PATTERN;
	char pol_copy[sizeof tmpdir + sizeof "/crypto-policies"];
	char ucp_basedir[sizeof "base_dir=" + sizeof pol_copy];
	char fake_fips_fname[sizeof tmpdir + sizeof "/fake_fips"];
	FILE* fake_fips_file;
	_Bool fake_fips = 0;
	const char* fake_fips_value = NULL;
	_Bool already_admin;

	if (argc < 3) {
		fputs("%s requires at least 2 arguments\n", stderr);
		fputs("Usage: runcp [--fake-fips=0|--fake-fips=1] POLICY cmd\n",
			stderr);
		exit(EXIT_FAILURE);
	}

	if (!strcmp(argv[1], "--fake-fips=0")) {
		fake_fips = 1;
		fake_fips_value = "0\n";
		fprintf(stderr,
			"Do not use --fake-fips=0 other than for testing.\n"
			"This option is, obviously, not FIPS-compliant.\n");
	} else if (!strcmp(argv[1], "--fake-fips=1")) {
		fake_fips = 1;
		fake_fips_value = "1\n";
		fprintf(stderr, "--fake-fips=1 is not proper FIPS mode\n");
	}

	if (mkdtemp(tmpdir) != tmpdir) {
		fprintf(stderr, "Failed to create temporary directory: %m\n");
		exit(EXIT_FAILURE);
	}

	run_cmd((char *[]){
			"/usr/bin/cp",
			"-a",
			"/etc/crypto-policies",
			tmpdir,
			NULL
		},
		(char *[]){ NULL });

	sprintf(pol_copy, "%s/%s", tmpdir, "crypto-policies");
	sprintf(ucp_basedir, "base_dir=%s", pol_copy);

	run_cmd((char *[]){
			"/usr/bin/update-crypto-policies",
			"--no-reload",
			"--set",
			argv[fake_fips ? 2 : 1],
			NULL
		},
		(char *[]){ ucp_basedir, NULL });

	if (fake_fips) {
		sprintf(fake_fips_fname, "%s/fake_fips", tmpdir);
		fake_fips_file = fopen(fake_fips_fname, "w");
		if (!fake_fips_file) {
			perror("Failed to open fake_fips for writing");
			exit(EXIT_FAILURE);
		}
		if (fputs(fake_fips_value, fake_fips_file) == EOF) {
			perror("Failed to write to fake_fips file");
			exit(EXIT_FAILURE);
		}
		fclose(fake_fips_file);
	}

	if (capng_get_caps_process() != 0) {
		perror("capng_get_caps_process");
		exit(EXIT_FAILURE);
	}

	already_admin = capng_have_capability(CAPNG_EFFECTIVE, CAP_SYS_ADMIN);

	if (!already_admin)
		set_admin(CAPNG_ADD);

	if (unshare(CLONE_NEWNS) != 0) {
		perror("unshare");
		exit(EXIT_FAILURE);
	}

	if (mount("none", "/", NULL, MS_REC | MS_PRIVATE, NULL) != 0) {
		fprintf(stderr, "Failed to make mount namespace private: %m\n");
		exit(EXIT_FAILURE);
	}

	if (mount(pol_copy, "/etc/crypto-policies", NULL, MS_BIND, NULL) != 0) {
		fprintf(stderr, "Failed to mount crypto policy copy: %m\n");
		exit(EXIT_FAILURE);
	}

	if (fake_fips) {
		if (mount(fake_fips_fname, "/proc/sys/crypto/fips_enabled",
				NULL, MS_BIND, NULL) != 0) {
			fprintf(stderr, "Failed to mount fips_enabled: %m\n");
			exit(EXIT_FAILURE);
		}
	}

	run_cmd(argv + (fake_fips ? 3 : 2), environ);

	if (fake_fips) {
		if (umount("/proc/sys/crypto/fips_enabled") != 0) {
			fprintf(stderr, "Failed to unmount fips_enabled: %m\n");
			exit(EXIT_FAILURE);
		}
	}

	if (umount("/etc/crypto-policies") != 0) {
		fprintf(stderr, "Failed to unmount crypto policy copy: %m\n");
		exit(EXIT_FAILURE);
	}

	if (!already_admin)
		set_admin(CAPNG_DROP);

	run_cmd((char *[]){ "/usr/bin/rm", "-r", tmpdir, NULL },
		(char *[]){ NULL });

	return EXIT_SUCCESS;
}

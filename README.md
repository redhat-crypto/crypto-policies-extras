# crypto-policies-extras

## COPR

Extra, unsupported tools to work with crypto-policies.

https://copr.fedorainfracloud.org/coprs/asosedkin/crypto-policies-extras

## runcp

`runcp` is used to execute a command under a set cryptographic policy.
Cryptographic policies define default algorithm selections
used for, e.g., SSL/TLS negotiation.

`runcp` is meant to be used as a testing tool
and isn't officially supported like the rest of the crypto-policies suite.

Attempt to connect to example.com using openssl under FUTURE policy:

`runcp FUTURE openssl s_client example.com:443`

List algorithms GnuTLS will negotiate under LEGACY policy
with AD-SUPPORT subpolicy applied on top:

`runcp LEGACY:AD-SUPPORT gnutls-cli --list`

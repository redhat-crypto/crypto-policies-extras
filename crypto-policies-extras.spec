### metapackage / shared ###

Name:		crypto-policies-extras
Version:	0.2
Release:	%{autorelease}
Summary:	Extra, unsupported tools to work with crypto-policies
License:	GPL-3.0-or-later
Source:		crypto-policies-extras-0.2.tar.gz
URL:		https://gitlab.com/redhat-crypto/crypto-policies-extras

Recommends:	crypto-policies-extras-runcp

%description
Meta-package recommending all other crypto-policies-extras-* packages


%prep
%autosetup -n crypto-policies-extras-0.2

%build
make %{?_smp_mflags}

%files
# intentionally left blank

%install
# -extras-runcp
install -m 0755 -d %{buildroot}%{_bindir}
install -m 0775 runcp %{buildroot}%{_bindir}/runcp
install -m 0755 -d %{buildroot}%{_mandir}
install -m 0755 -d %{buildroot}%{_mandir}/man8
install -m 0644 runcp.8 %{buildroot}%{_mandir}/man8/runcp.8


### crypto-policies-extras-runcp ###

%package runcp
Summary:	Set crypto-policy for a process
BuildRequires:	asciidoc
BuildRequires:	libcap-ng-devel
BuildRequires:	libxslt
BuildRequires:	gcc
BuildRequires:	make
Requires:	crypto-policies
Requires:	/usr/bin/update-crypto-policies
%description runcp
A tool to set crypto-policy for specific processes.
Originally written by Ian Pilcher and published at
https://bugzilla.redhat.com/show_bug.cgi?id=2064740
%files runcp
%{_bindir}/runcp
%{_mandir}/man8/runcp.8.gz
%post runcp
setcap cap_sys_admin=p %{_bindir}/runcp


### changelog ###

%changelog
%autochangelog
